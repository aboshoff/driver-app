```js noeditor
<section className="section section--restaurant">
  <h3 className="section__title">Fancy Restaurant</h3>
  <p className="section__paragraph">123 The Crescent, Cape Town, 7550</p>
  <Restaurant restaurant={order}/>
</section>
```

```js static
<Restaurant restaurant={order}/>
```