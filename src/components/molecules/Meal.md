```js noeditor
<div className="meal">
  <p className="meal__name">Meal title</p>
  <div className="eyebrow">Comes with fries</div>
</div>
```

```js static
<Meal meal={meal}/>
```