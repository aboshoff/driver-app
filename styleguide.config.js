module.exports = {
  components: [
    'src/components/atoms/*.js',
    'src/components/molecules/*.js',
    'src/components/organisms/*.js',
  ]
};